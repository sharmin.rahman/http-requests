# Login Form with Express.js

Simple login form done with express.js. Barebone structure was covered in class. Added Materialize framework to give it a solid look!

## To test the app:

- In terminal on the folder execute `npm init`
- Afterwards install express by executing `npm install express`
- Finally for rendering I used ejs so install it by executing `npm install ejs`

Once the dependencies have been installed, run the server by executing `node index`

- The users.json file contains few username password pairs which if used in the login form will welcome the user to the website
- Otherwise user will be prompted that the access info is not correct

In case users.json file isn't accessible; here are the username password combinations:

Username: John, PW: 1234
Username: Mike, PW: 5678
Username: Mary, PW: 9090
Username: Pepi, PW: 0000
